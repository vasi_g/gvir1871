package gvir1871MV.repository;

import java.util.List;

import gvir1871MV.utils.ClasaException;

import gvir1871MV.model.Nota;

public interface NoteRepository {
	
	public void addNota(Nota nota) throws ClasaException;
	public List<Nota> getNote(); 
	public void readNote(String fisier);
	
}
