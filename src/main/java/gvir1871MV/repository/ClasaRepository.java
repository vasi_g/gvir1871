package gvir1871MV.repository;

import java.util.HashMap;
import java.util.List;

import gvir1871MV.utils.ClasaException;

import gvir1871MV.model.Corigent;
import gvir1871MV.model.Elev;
import gvir1871MV.model.Medie;
import gvir1871MV.model.Nota;

public interface ClasaRepository {
	
	public void creazaClasa(List<Elev> elevi, List<Nota> note);
	public HashMap<Elev, HashMap<String, List<Double>>> getClasa();
	public List<Medie> calculeazaMedii() throws ClasaException;
	public void afiseazaClasa();
	public List<Corigent> getCorigenti();
}
