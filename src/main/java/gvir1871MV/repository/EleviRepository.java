package gvir1871MV.repository;

import java.util.List;

import gvir1871MV.model.Elev;

public interface EleviRepository {
	public void addElev(Elev e);
	public List<Elev> getElevi();
	public void readElevi(String fisier);
}
